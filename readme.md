# Node Task

Hi!

I'm doing this all by my self and I will succeed! <bt>

<br>**Just remember**
- The only difference between a good day and a bad day is ***your*** attitude!
- A bad attitude is like a flat tire. You can't go anywhere until ***you*** change it. 
  
<br>![alt text](steps.jpg)<br><br><br>

## Some of my favorite quotes too..

>"I can" is 100 times more important than 'I.Q'"

>"The world is changed by your example, not by your opinion."

<br><br><br>

# Cleaning my life
<br>

## My Table of happiness and joy
<br>

| Thing in my life      | make me happy | worth to save |
| ----------- | ----------- | ----------- |
| family      | x      | x |
| real friends   | x        | x |
| hiking in the woods   | x        | x |
| working as a teacher   |         |  |
<br><br>
## Things I need to do to for my happiness
- [x] Get rid of the teaching business
- [ ] Plan a new hiking trip in February
- [ ] Build a snow castle with your family
<br><br>
You can add new line to task list above just adding `- [ ]` before the task.<br><br>
>## So lets continue "to do"
>
>
>1. Call all hiking friend and set a date
>    1. ask if anyone needs a small tent
>    2. ask if anyone can lend us snowshoes
>2. Find some inspirational pictures for the snow castle

